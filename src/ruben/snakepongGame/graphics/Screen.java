package ruben.snakepongGame.graphics;

import ruben.snakepongGame.entities.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * 
 * @author Ruben Heintjens (inspired by snake tutorial by pj6444 on youtube)
 *
 */

public class Screen extends JPanel implements Runnable {
	
	private static final long serialVersionUID = 1L;
	
	public static final int WIDTH = 800, HEIGHT = 800;
	public int SCORE = 0;
	public int collision = 0;
	private Thread thread;
	private boolean running = false;
	
	private Point p;
	private LinkedList<Point> snake;
	private LinkedList<Point> paddle;
	private Point ball;
	
	private Random r;
	
	
	
	private Apple apple;
	
	private int x = 10, y = 10, xx , yy , xp, yp;
	private int size = 5;
	
	private int ticks = 0, ticks2 = 0, ticks3 = 0;
	
	private Key key;
	
	// direction of the snake
	private boolean right = true, left = false, up = false, down = false;
	
	//Whether the game is over or not
	private boolean over = false;
	
	// paddle direction
	private boolean pUP = true, pDOWN = false;
	
	// ball direction example: Ball Up Left = bul
	private boolean bul = false, bur = true, bdl = false, bdr = false;
	
	/**
	 * constructor for the screen
	 */
	public Screen() {
		setFocusable (true);
		key = new Key();
		addKeyListener(key);
		setPreferredSize( new Dimension(WIDTH, HEIGHT));
		//random to place a apple randomly on the screen (not yet implemented)
		r = new Random ();
		
		//coordinates used for the paddle
		int xx=0, yy=10;
		
		snake = new LinkedList<Point>();
		paddle = new LinkedList<Point>();
		ball = new Point (10, 10, 10);
		
		for (int i=0; i<5;i++) {
			yy++;
			this.yy = yy;
			Point p = new Point (0,yy, 10);
			paddle.prepend(p);
		
		}
		
		
		start();
	}
	/**
	 * this method controls the propagation of the game, all movements, collisions,... see README uploadzone voor verduidelijking
	 */
	public void tick() {
		ticks++;
		ticks2++;
		ticks3++;
		
		if (snake.getSize() == 0) {
			p= new Point (x,y,10);
			this.snake = new LinkedList<Point>(p);
			}
		
		if (x<0 || x>79 || y<0 || y>79) {
			stop();
		}
		
		if (ticks > 1000000) {
			if (right) {x++;}
			
			if (left) {x--;}
			
			if (up) {y--;}
			
			if (down) { y++;}
			
			p = new Point (x,y,10);
			snake.prepend(p);
			
			ticks = 0;
			
			if (snake.getSize() > SCORE+5) {
				snake.removeTail(snake.getTailp());
					
			}
		}
		if (ticks2 > 5000000) {
			
			ticks2=0;
			if (pUP) {
				yy--;
			}
			else {
				pUP = false;
				pDOWN = true;
			}
			if (pDOWN) {
				yy++;
			}
			if (yy==0) {
				pUP = false;
				pDOWN = true;
				ticks2+=5;
			}
			if (yy==80) {
				pDOWN = false;
				pUP = true;
				ticks2+=5;
			}
			
			p = new Point (0,yy,10);
			paddle.prepend(p);
			
			if (paddle.getSize() > this.size) {
				paddle.removeTail(paddle.getTailp());
			}
			ticks2 = 0;
		}
		
		if (ticks3 > 10000000) {
			
			ticks3=0;
			 xp = ball.getX();
			 yp = ball.getY();
			 
			if(bul) {
				if (xp-1>=0 && yp-1>=0) {
					Point coll = new Point (x-1,y,10);
					for (Point p: paddle) {
						if (coll == p) {
							if(pUP) {
								bul = false;
								bur = true;
								collision++;
								}
							if(pDOWN) {
								bul = false;
								bdr = true;
								collision++;
								}
							}
						}
				}
				if (xp-1 == 0) {
					SCORE++;
					this.ball = new Point (1,1,10);
				}
				if (yp-1 == 0) {
					bul = false;
					bdl = true;
				}
			}
			
			if(bdl) {
				if (xp-1>0 && yp < 79) {
					Point coll = new Point (x-1,y,10);
					for (Point p: paddle) {
						if (coll == p) {
							if(pUP) {
								bur = true;
								bdl = false;
								collision++;
								}
							if(pDOWN) {
								bdr = true;
								bdl = false;
								collision++;
								}
							}
						}
				}
				if (xp-1 == 0) {
					SCORE++;
					this.ball = new Point (1,1,10);
				}
				if (yp == 79) {
					bdl = false;
					bul = true;
				}
			}
			
			if(bur) {
				if(yp-1 == 0) {
					bur= false;
					bdr = true;
				}
				if (xp+1>79) {
					bur = false;
					bul = true;
				}
				if (xp+1<79 && yp-1>0) {
					Point col = new Point (xp+1,yp,10);
					for (Point p: snake) {
						if (col == p) {
							if (up) {
								bur = false;
								bul = true;
								collision++;
							}
							if (down) {
								bur = false;
								bdl = true;
								collision++;
							}
							else {
								bur = false;
								bul = true;
								collision++;
							}
							
							}
					
						}
					}
				}
			
			if(bdr) {
				if(yp+1 > 79) {
					bdr= false;
					bur = true;
				}
				if (xp+1>79) {
					bdr = false;
					bdl = true;
				}
				if (xp+1<79 && yp+1<79) {
					Point col = new Point (xp+1,yp,10);
					for (Point p: snake) {
						if (col == p) {
							if (up) {
								bdr = false;
								bul = true;
								collision++;
							}
							if (down) {
								bdr = false;
								bdl = true;
								collision++;
							}
							else {
								bdr = false;
								bdl = true;
								collision++;
							}
							
							}
						else{bdr = true;}
						}
					}
				
				}
			
			
		if(bur)	{
			ball.setX(ball.getX()+1);
			ball.setY(ball.getY()-1);
		}
		if(bul)	{
			ball.setX(ball.getX()-1);
			ball.setY(ball.getY()-1);
		}
		if(bdr)	{
			ball.setX(ball.getX()+1);
			ball.setY(ball.getY()+1);
		}
		if(bdl)	{
			ball.setX(ball.getX()-1);
			ball.setY(ball.getY()+1);
		}
			
			
			
			
		}
		
	}
	/**
	 * this method paints the screen by calling the draw method from the Point class
	 */
	public void paint(Graphics g) {
		
		g.clearRect(0,0,WIDTH, HEIGHT);
		
		g.setColor(new Color (10,50,0));
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		int counter = 0;
		
		for(Point p : snake) {
			if (p== snake.getHead()) {
				p.drawHead(g);
			}
			else {
				p.draw(g);
				
			}
			counter++;
			}
		for(Point p : paddle) {
			p.draw(g);
			counter++;
		}
		
		ball.draw(g);
		
		
	}
	
	/**
	 * starts the game
	 */
	public void start() {
		running = true;
		thread = new Thread(this, "Game Loop");
		thread.start();
		over = false;
		
	}
	
	/**
	 * stops the game
	 */
	public void stop() {
		running = false;
		JOptionPane.showMessageDialog(null, "Uw score is: "+SCORE+" en U had: "+collision+" bodsingen");
		
		
	}
	
	/**
	 * calls the tick and repaint methods when the game is running
	 */
	public void run() {
		while (running) {
			tick();
			repaint();
			
		}
		
	}
	/**
	 * is responsible for the control over the movement of the snake with the arrow key's
	 * @author Ruben Heintjens (inspired by snake tutorial by pj6444 on youtube)
	 *
	 */
	private class Key implements KeyListener{

	/**
	 * looks at the key that is pressed and acts accordingly
	 */
		public void keyPressed(KeyEvent e) {
			int key = e.getKeyCode();
			
			if (key == KeyEvent.VK_RIGHT && !left) {
				up= false;
				down= false;
				right= true;
				
			}
			if (key == KeyEvent.VK_LEFT && !right) {
				up= false;
				down= false;
				left= true;
				
			}
			if (key == KeyEvent.VK_UP && !down) {
				left=false;
				right=false;
				up=true;
			}
			if (key == KeyEvent.VK_DOWN && !up) {
				left=false;
				right=false;
				down=true;
			}
		
			
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}

}
