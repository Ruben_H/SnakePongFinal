package ruben.snakepongGame.entities;

import java.awt.Color;
import java.awt.Graphics;
/**
 * this class is used as "part" of the snake and paddle
 * @author Ruben Heintjens
 *
 */
public class Point {
	
	private int x, y, width, height;
	
	
	/**
	 * constructor for a point
	 * @param x coordinate
	 * @param y coordinate
	 * @param partSize size of the block drawn on the screen
	 */
	public Point (int x, int y, int partSize) {
		this.x = x;
		this.y = y;
		width = partSize;
		height = partSize;
	}
	
	public void tick() {
		
	}
	/**
	 * tells the repaint method in screen how a point should be painted
	 * @param g
	 */
	public void draw(Graphics g){
		g.setColor(Color.BLACK);
		g.fillRect(x*width, y*height, width, height);
		g.setColor(Color.GREEN);
		g.fillRect(x*width+2, y*height +2, width-4, height-4);
		}
	public void drawHead(Graphics g){
		g.setColor(Color.BLACK);
		g.fillRect(x*width, y*height, width, height);
		g.setColor(Color.BLACK);
		g.fillRect(x*width+2, y*height +2, width-4, height-4);
		}
	
	/**
	 * getter for the x coordinate
	 * @return the x coordinate
	 */
	public int getX() {
		return this.x;
	}
	
	/**
	 * getter for the y coordinate
	 * @return the y coordinate
	 */
	public int getY() {
		return this.y;
	}
	
	/**
	 *setter for the x coordinate
	 * 
	 */
	public void setX(int x) {
		this.x = x;
		
	}
	
	/**
	 *setter for the y coordinate
	 * 
	 */
	public void setY(int y) {
		this.y = y;
		
	}
	

}