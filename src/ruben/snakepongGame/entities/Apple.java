package ruben.snakepongGame.entities;


import java.awt.Color;
import java.awt.Graphics;

/**
 * class that generates the apple for the snake to eat
 * @author Ruben Heintjens (inspired by snake tutorial by pj6444 on youtube)
 *
 */
public class Apple {
	
	
	private int x, y, width, height;
	
/*
 * constructor for an apple
 */
	public Apple (int x, int y, int partsize) {
		this.x = x;
		this.y = y;
		width = partsize;
		height = partsize;
	}
	
	public void tick() {
		
	}
	
/**
 * tells the repaint method in screen how an apple should be painted
 * @param g
 */
	public void draw (Graphics g) {
		g.setColor(Color.RED);
		g.fillRect(x*width, y*height, width, height);
		
		
	}
	
	
// getters and setters for Y and X	
	public int GetX() {
		return this.x;
	}
	public int GetY() {
		return this.y;
	}
	
	public void setX(int x) {
		this.x = x;
		
	}
	public void setY(int y) {
		this.y = y;
		
	}

}
