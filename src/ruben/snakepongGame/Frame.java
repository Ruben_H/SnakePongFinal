package ruben.snakepongGame;

import java.awt.GridLayout;

import javax.swing.JFrame;

import ruben.snakepongGame.graphics.Screen;

/**
 * 
 * @author Ruben Heintjens (inspired by snake tutorial by pj6444 on youtube)
 *
 */
public class Frame extends JFrame {
	/**
	 * opens a new frame
	 */
	public Frame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("SnakePong");
		setResizable(false);
		
		init();
		
		
	}
	
/**
 * initiates a layout, opens a instance of screen and adds it to the frame
 */
	public void init() {
		setLayout( new GridLayout(1,1,0,0));
		
		Screen s = new Screen();
		add(s);
		
		pack();
		
		setLocationRelativeTo(null);
		setVisible(true);
		
		
	}
	/**
	 * main method
	 * @param args
	 */
	public static void main(String[] args) {
		new Frame();
	}
}
